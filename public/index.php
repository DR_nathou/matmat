<?php
require '../vendor/autoload.php';


session_start();


$app = new \Slim\App(['settings' => ['displayErrorDetails' => true]]);



require('../App/container.php');

//$app->get('/', \App\Controllers\Home_Controller::class . ':logout')->setName('logout');
$app->get('/', \App\Controllers\Login_Controller::class . ':login');
$app->get('/contact', \App\Controllers\PagesController::class . ':contact')->setName('contact');

$app->get('/test', \App\Controllers\PagesController::class . ':show');

$app->post('/contact', \App\Controllers\PagesController::class . ':contact');
$app->post('/modifier', \App\Controllers\PagesController::class . ':updateButton')->setName('modifier');
$app->post('/delete', \App\Controllers\PagesController::class . ':delete')->setName('delete');
$app->post('/modNew', \App\Controllers\PagesController::class . ':updateNew')->setName('updateNew');
$app->post('/loging_in', \App\Controllers\Login_Controller::class . ':logingin')->setName('loging_in');;

$app->get('/logout', \App\Controllers\Login_Controller::class . ':logout')->setName('logout');
$app->get('/home', \App\Controllers\Home_Controller::class . ':home')->setName('home');
$app->post('/home', \App\Controllers\Home_Controller::class . ':home')->setName('home');

$app->get('/fourniture', \App\Controllers\Fourniture_Controller::class . ':fourniture')->setName('fourniture');
$app->post('/fourniture-modifier', \App\Controllers\Fourniture_Controller::class . ':modifier')->setName('fourniture-modifier');
$app->post('/fourniture-buttons', \App\Controllers\Fourniture_Controller::class . ':buttons')->setName('fourniture-head-buttons');

$app->post('/fourniture-details', \App\Controllers\Fourniture_Controller::class . ':details')->setName('fourniture-details');
$app->get('/fourniture-nouveau', \App\Controllers\Fourniture_Controller::class . ':nouveau')->setName('fourniture-nouveau');
$app->post('/fourniture-enregistrer', \App\Controllers\Fourniture_Controller::class . ':enregistrer')->setName('fourniture-enregistrer');



$app->get('/utilisateur', \App\Controllers\Utilisateur_Controller::class . ':utilisateur')->setName('utilisateur');
$app->post('/utilisateur-buttons', \App\Controllers\Utilisateur_Controller::class . ':buttons')->setName('utilisateur-head-buttons');
$app->post('/utilisateur-details', \App\Controllers\Utilisateur_Controller::class . ':details')->setName('utilisateur-details');
$app->post('/utilisateur-modifier', \App\Controllers\Utilisateur_Controller::class . ':modifier')->setName('utilisateur-modifier');


$app->get('/salle', \App\Controllers\Salle_Controller::class . ':salle')->setName('salle');
$app->post('/salle-details', \App\Controllers\Salle_Controller::class . ':details')->setName('salle-details');
$app->post('/salle-modifier', \App\Controllers\Salle_Controller::class . ':modifier')->setName('salle-modifier');
$app->post('/salle-buttons', \App\Controllers\Salle_Controller::class . ':buttons')->setName('salle-head-buttons');
$app->get('/salle-nouveau', \App\Controllers\Salle_Controller::class . ':nouveau')->setName('salle-nouveau');
$app->post('/salle-enregistrer', \App\Controllers\Salle_Controller::class . ':enregistrer')->setName('salle-enregistrer');


$app->get('/type-salle-nouveau', \App\Controllers\TypeSalle_Controller::class . ':nouveau')->setName('type-salle-nouveau');
$app->post('/type-salle-enregistrer', \App\Controllers\TypeSalle_Controller::class . ':enregistrer')->setName('type-salle-enregistrer');
$app->post('/type-salle-modifier', \App\Controllers\TypeSalle_Controller::class . ':modifier')->setName('type-salle-modifier');
$app->post('/type-salle-changeNom', \App\Controllers\TypeSalle_Controller::class . ':changeNom')->setName('type-salle-changeNom');

$app->get('/type-fourniture-nouveau', \App\Controllers\Type_Controller::class . ':nouveau')->setName('type-fourniture-nouveau');
$app->post('/type-fourniture-enregistrer', \App\Controllers\Type_Controller::class . ':enregistrer')->setName('type-fourniture-enregistrer');
$app->post('/type-fourniture-modifier', \App\Controllers\Type_Controller::class . ':modifier')->setName('type-fourniture-modifier');
$app->post('/type-fourniture-changeNom', \App\Controllers\Type_Controller::class . ':changeNom')->setName('type-fourniture-changeNom');





$app->get('/profile', \App\Controllers\Profile_Controller::class . ':profile')->setName('profile');

$app->run();

