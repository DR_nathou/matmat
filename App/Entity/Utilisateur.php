<?php
/**
 * Created by PhpStorm.
 * User: drznathou
 * Date: 24/05/2017
 * Time: 02:28
 */

namespace App\Entity;


class Utilisateur
{
   public $id;

   public $adresse_id;

   public $username;

   public $email;

   public $enabled;

   public $password;

   public $last_login;

   public $password_request_at;

   public $roles;

   public $nom;

   public $prenom;

   public $Adresse;

   public $Fourniture;

    public function getId() {
        return $this->id;
    }

    public function getNom() {
        return $this->nom;
    }

    public function getAdresse() {
        return $this->adresse;
    }

    public function getUsername() {
        return $this->username;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getEnabled() {
        return $this->enabled;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getLast_login() {
        return $this->last_login;
    }

    public function getPassword_request_at() {
        return $this->password_request_at;
    }
    
    public function getRoles() {
        return $this->roles;
    }

    public function getPrenom() {
        return $this->prenom;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setNom($nom) {
        $this->nom = $nom;
    }

    public function setAdresse(Adresse $adresse) {
        $this->adresse = $adresse;
    }

    public function setUsername($username) {
        $this->username= $username ;
    }

    public function setEmail($email) {
        $this->email= $email ;
    }

    public function setEnabled($enabled) {
        $this->enabled= $enabled ;
    }

    public function setPassword($password) {
        $this->password= $password ;
    }

    public function setLast_login($last_login) {
        $this->last_login= $last_login ;
    }

    public function setPassword_request_at($password_request_at) {
        $this->password_request_at= $password_request_at ;
    }

    public function setRoles($roles) {
        $this->roles= $roles ;
    }

    public function setPrenom($prenom) {
        $this->prenom= $prenom ;
    }


}