<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Entity;

use App\Entity\Description;
use App\Entity\Adresse;

class Fourniture {
    

    public $id;

    public $marque;
    

    public $modele;
    

    public $code;
    

    public $utilisateur_id;
    

    public $salle_id;
    

    public $description_id;

    public $Utilisateur;

    public $Salle;

    public $Description;

    public function getId() {
        return $this->id;
    }

    public function getMarque() {
        return $this->marque;
    }

    public function getModele() {
        return $this->modele;
    }

    public function getCode() {
        return $this->code;
    }


    public function setId($id) {
        $this->id = $id;
    }

    public function setMarque($marque) {
        $this->marque = $marque;
    }

    public function setModele($modele) {
        $this->modele = $modele;
    }

    public function setCode($code) {
        $this->code = $code;
    }

    public function setUser(User $user) {
        $this->user = $user;
    }

    public function setSalle(Salle $salle) {
        $this->salle = $salle;
    }

    public function setDescription(Description $description) {
        $this->description = $description;
    }


    
}
