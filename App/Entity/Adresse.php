<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Entity;


class Adresse {
    

    public $id;

    public $rue;

    public $numero;

    public $code;

    public $ville;

 /*   public function __construct($id, $rue, $numero, $code, $ville)
    {
        $this->id = $id;
        $this->rue = $rue;
        $this->numero = $numero;
        $this->code = $code;
        $this->ville = $ville;
    }*/

    public function getId() {
        return $this->id;
    }

    public function getRue() {
        return $this->rue;
    }

    public function getNumero() {
        return $this->numero;
    }

    public function getCode() {
        return $this->code;
    }

    public function getVille() {
        return $this->ville;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setRue($rue) {
        $this->rue = $rue;
    }

    public function setNumero($numero) {
        $this->numero = $numero;
    }

    public function setCode($code) {
        $this->code = $code;
    }

    public function setVille($ville) {
        $this->ville = $ville;
    }


}
