<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Entity;

use App\Entity\Type;

class Description {
    

    public $id;
    

    public $nom;
    

    public $description;
    
    public $type_id;

    public $Type;
    
    public function getId() {
        return $this->id;
    }

    public function getNom() {
        return $this->nom;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getType(){
        return $this->type;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setNom($nom) {
        $this->nom = $nom;
    }

    public function setDescription(Description $description) {
        $this->description = $description;
    }

    public function setType($type) {
        $this->type = $type;
    }



}
