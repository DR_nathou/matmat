<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Entity;

use App\Entity\Adresse;
use App\Entity\TypeSalle;


class Salle {
   

    public $id;
    

    public $aile;

    public $etage;
    

    public $numero;
    

    public $nom;
    

    public $type_salle_id;

    public $TypeSalle;
    
    public $adresse_id;

    public $Adresse;

    public $Fourniture;
    
    public function getId() {
        return $this->id;
    }

    public function getAile() {
        return $this->aile;
    }

    public function getEtage() {
        return $this->etage;
    }

    public function getNumero() {
        return $this->numero;
    }

    public function getNom() {
        return $this->nom;
    }


    public function setId($id) {
        $this->id = $id;
    }

    public function setAile($aile) {
        $this->aile = $aile;
    }

    public function setEtage($etage) {
        $this->etage = $etage;
    }

    public function setNumero($numero) {
        $this->numero = $numero;
    }

    public function setNom($nom) {
        $this->nom = $nom;
    }

    public function setTypeSalle(TypeSalle $typeSalle) {
        $this->typeSalle = $typeSalle;
    }

    public function setAdresse(Adresse $adresse) {
        $this->adresse = $adresse;
    }


}
