<?php

namespace App\Common\BarCode\Exceptions;

class UnknownTypeException extends BarcodeException {}