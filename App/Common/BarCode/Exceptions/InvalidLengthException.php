<?php

namespace App\Common\BarCode\Exceptions;

class InvalidLengthException extends BarcodeException {}