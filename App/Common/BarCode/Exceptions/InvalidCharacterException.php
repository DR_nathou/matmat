<?php

namespace App\Common\BarCode\Exceptions;

class InvalidCharacterException extends BarcodeException {}