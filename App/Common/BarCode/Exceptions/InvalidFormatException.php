<?php

namespace App\Common\BarCode\Exceptions;

class InvalidFormatException extends BarcodeException {}