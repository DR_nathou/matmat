<?php

namespace App\Common\BarCode\Exceptions;

class InvalidCheckDigitException extends BarcodeException {}