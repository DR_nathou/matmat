<?php
/**
 * Created by PhpStorm.
 * User: drznathou
 * Date: 26/05/2017
 * Time: 03:33
 */

namespace App\Managers;

use App\Common\BarCode\BarcodeGeneratorPNG;
use PHPUnit_Framework_TestCase;

class BarCode_Manager
{

    public static function generateFromNumber($number)
    {
        $png = new BarcodeGeneratorPNG();
        return $png->getBarcode($number, 'EAN2');
    }


}