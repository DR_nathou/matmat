<?php
/**
 * Created by PhpStorm.
 * User: drznathou
 * Date: 24/05/2017
 * Time: 02:07
 */

namespace App\Controllers;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

use Slim\App;
use  Slim\Views\Twig;


class Home_Controller extends Controller
{
    public function home($request, $response)
    {
        if (!$_SESSION['is_open'])
        {
            echo "<script type='text/javascript'>alert('" . 'Veuillez vous connecter!' . "')</script>";
            $lc = new Login_Controller($this->container);
            $lc->login($request, $response);
        }
        else $this->render($response, 'pages/home.twig',array('data' => $this->getData()));
    }

    private function getData()
    {
        try
        {
            $utilisateur = $this->container['pdo']->query('SELECT nom, f.utilisateur_id from Utilisateur u left JOIN Fourniture f on f.utilisateur_id = u.id where u.enabled = 1 and f.utilisateur_id IS null')->fetchAll($this->container['pdo']::FETCH_CLASS, '\App\Entity\Utilisateur');
            $fourniture = $this->container['pdo']->query('SELECT nom, f.utilisateur_id from Utilisateur u right JOIN Fourniture f on f.utilisateur_id = u.id where f.utilisateur_id IS null')->fetchAll($this->container['pdo']::FETCH_CLASS, '\App\Entity\Fourniture');

            return array(count($utilisateur), count($fourniture));
        }
        catch(\PDOException $e)
        {
            return $e;
        }
    }

}