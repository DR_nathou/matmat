<?php
/**
 * Created by PhpStorm.
 * User: drznathou
 * Date: 26/05/2017
 * Time: 04:02
 */

namespace App\Controllers;


class Description_Controller extends Controller
{
    public function describeYourSelf($id)
    {
        try
        {
            $query = $this->container['pdo']->prepare('SELECT id, type_id, nom, description FROM Description WHERE id = ?');
            $query->execute([$id]);
            $description = $query->fetchAll($this->container['pdo']::FETCH_CLASS, '\App\Entity\Description');
            $tc = new Type_Controller($this->container);
            if(isset($description[0]))
            {
                $description[0]->Type = $tc->describeYourSelf($description[0]->type_id);
                return $description[0];
            }
        }
        catch(\PDOException $e)
        {
            return "erreur: ".$e;
        }
    }

    public function modifier()
    {
        try
        {
            $query = $this->container['pdo']->prepare('UPDATE Description set description = ?, nom = ?, type_id = ? WHERE id = ?');
            $query->execute([$_POST["description"], $_POST["description_nom"], $_POST["type"], $_POST["description_id"]]);

        }
        catch(\PDOException $e)
        {
            return $e;
        }
    }

    public function enregistrer()
    {
        try
        {
            $query = $this->container['pdo']->prepare('INSERT INTO  Description ( type_id ,  nom ,  description ) VALUES (?, ?, ?)');
            $query->execute([$_POST['type'], $_POST['description_nom'], $_POST['description']]);

            $query = $this->container['pdo']->prepare('SELECT id FROM Description WHERE nom = ?');
            $query->execute([$_POST['description_nom']]);
            $description = $query->fetchAll($this->container['pdo']::FETCH_CLASS, '\App\Entity\Description');
            return $description[0]->id;

        }
        catch(\PDOException $e)
        {
            return $e;
        }
    }

}