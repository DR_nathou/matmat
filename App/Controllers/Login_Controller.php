<?php
namespace App\Controllers;
/**
 * Created by PhpStorm.
 * User: drznathou
 * Date: 06/03/2017
 * Time: 14:18
 *
 */
session_start();
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

use Slim\App;
use  Slim\Views\Twig;


class Login_Controller extends Controller
{
    public function login($request, $response, $msg = 'Veuillez vous connecter', $pseudo = '')
    {
        $this->render($response, 'pages/login.twig',
            array('msg' => $msg, 'pseudo' => $pseudo));
    }


    public function logingin($request, $response)
    {
        try
        {
            $query = $this->container['pdo']->prepare('select * from Utilisateur where username = ? or email = ?');
            $query->execute([$_POST['pseudo'], $_POST['pseudo']]);
            $user = $query->fetchAll($this->container['pdo']::FETCH_CLASS, '\App\Entity\Utilisateur');
            if($user[0] == null) $this->login($request, $response, 'Adresse email ou pseudo incorrect');
            elseif($user[0]->password != $_POST['motdepasse']) $this->login($request, $response, 'Mot de passe incorrect', $_POST['pseudo']);
            else
            {
                $_SESSION['is_open'] = true;
                $hc = new Home_Controller($this->container);
                $hc->home($request, $response);
            }
        }
        catch(\PDOException $e)
        {
            return $e->getMessage();
        }
    }

    public function logout($request, $response)
    {
        $_SESSION['is_open'] = false;
        $this->login($request, $response);
    }

}