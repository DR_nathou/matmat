<?php
/**
 * Created by PhpStorm.
 * User: drznathou
 * Date: 26/05/2017
 * Time: 04:01
 */

namespace App\Controllers;


class Adresse_Controller extends Controller
{
    public function describeYourSelf($id)
    {
        try
        {
            $query = $this->container['pdo']->prepare('SELECT id, numero, rue, code, ville FROM Adresse WHERE id = ?');
            $query->execute([$id]);
            $Adresse = $query->fetchAll($this->container['pdo']::FETCH_CLASS, '\App\Entity\Adresse');
            return array('id' => $Adresse[0]->id, 'numero' => $Adresse[0]->numero, 'rue' => $Adresse[0]->rue, 'ville' => $Adresse[0]->ville, 'code' => $Adresse[0]->code);

        }
        catch(\PDOException $e)
        {
            return "erreur: ".$e;
        }
    }

    public function modifier($request, $response)
    {
        try
        {
            $query = $this->container['pdo']->prepare('UPDATE Adresse SET numero = ?, rue = ?, code = ?, ville = ? WHERE id = ?');
            $query->execute([$_POST['Adresse_numero'], $_POST['Adresse_rue'], $_POST['Adresse_code'], $_POST['Adresse_ville'],$_POST['Adresse_id']]);
        }
        catch(\PDOException $e)
        {
            return "erreur: ".$e;
        }
    }

    public function enregistrer($request, $response)
    {
        try
        {
            $query = $this->container['pdo']->prepare('INSERT INTO  Adresse ( rue, numero, code, ville) VALUES (?, ?, ?, ?)');
            $query->execute([$_POST['rue'], $_POST['numero'], $_POST['code'], $_POST['ville']]);

            $query = $this->container['pdo']->prepare('select id FROM Adresse where rue = ?, numero = ?, code = ?, ville = ?');
            $query->execute([$_POST['rue'], $_POST['numero'], $_POST['code'], $_POST['ville']]);
            $ad = $query->fetchAll($this->container['pdo']::FETCH_CLASS, '\App\Entity\Adresse');
            return $ad[0]->id;
        }
        catch(\PDOException $e)
        {
            return $e;
        }
    }
}