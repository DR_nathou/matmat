<?php

namespace App\Controllers;
/**
 * Created by PhpStorm.
 * User: drznathou
 * Date: 06/03/2017
 * Time: 14:18
 *
 */

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

use Slim\App;
use  Slim\Views\Twig;

class PagesController extends Controller
{
    private function f5()
    {
        return Contact_Manager::_get_contact($this->container);
    }

    private function buildContact($new = false)
    {
        try
        {
            if($new)
            return new \App\DD\Contact($_POST['nom'], $_POST['prenom'], $_POST['email'], $_POST['adresse'], $_POST['telephone'], 0,"unknown", $this->container);
            else return new \App\DD\Contact($_POST['nom'], $_POST['prenom'], $_POST['email'], $_POST['adresse'], $_POST['telephone'], 0, $_POST['idcontact'], $this->container);

        }
        catch (\InvalidArgumentException $e)
        {
            echo "<script type='text/javascript'>alert('failed!')</script>";
        }
    }

    public function contactz($request, $response)
    {
        $this->render($response, 'pages/contact.twig',
            array('contactList' => self::f5()));
    }

    public function logout($request, $response)
    {
        $this->render($response, 'pages/login.twig',array());
    }

    public function delete($request, $response)
    {
        $error = Contact_Manager::_delete(self::buildContact());
        $this->render($response, 'pages/contact.twig',
            array('contactList' =>self::f5()));
        echo "<script type='text/javascript'>alert('".$error."')</script>";
    }

    public function updateButton($request, $response)
    {
        $this->render($response, 'pages/updateNew.twig',
            array('contact' => self::buildContact()));
    }

    public function updateNew($request, $response)
    {
        if($_POST['submit'] == 'Enregistrer')
        {
            $error = Contact_Manager::_update(self::buildContact());
        }
        elseif($_POST['submit'] == 'Nouveau')
        {
            $error = Contact_Manager::_new(self::buildContact(true));
        }
        else
        {
            $error['msg'] = "modifications annulées!";
            $error['good'] = true;
        }

        if($error['good']) $this->render($response, 'pages/contact.twig',
            array('contactList' => self::f5()));
        else $this->render($response, 'pages/updateNew.twig',
        array('contact' => self::buildContact()));


        echo "<script type='text/javascript'>alert('".$error['msg']."')</script>";
    }

    private function buildLogin()
    {
        return new \App\DD\Login($_POST['pseudo'], $_POST['motdepasse'], $this->container);
    }

    public function login($request, $response)
    {
        if($_POST['submit'] == 'Connecter')
        {
            $error = Login_Manager::_existingUser(self::buildLogin());
        }
        else $error = Login_Manager::_newUser(self::buildLogin());

        if($error['good']) $this->render($response, 'pages/contact.twig',
            array('contactList' => self::f5()));
        else $this->render($response, 'pages/login.twig', array());
        echo "<script type='text/javascript'>alert('".$error['msg']."')</script>";
    }

    public function contact($request, $response)
    {
        $this->render($response, 'pages/contact.twig',
            array());
    }

}