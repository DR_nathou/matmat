<?php
/**
 * Created by PhpStorm.
 * User: drznathou
 * Date: 24/05/2017
 * Time: 13:23
 */

namespace App\Controllers;
session_start();

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

use Slim\App;
use  Slim\Views\Twig;

class Utilisateur_Controller extends Controller
{
    public function utilisateur($request, $response)
    {
        if (!$_SESSION['is_open'])
        {
            echo "<script type='text/javascript'>alert('" . 'Veuillez vous connecter!' . "')</script>";
            $lc = new Login_Controller($this->container);
            $lc->login($request, $response);
        }
        else $this->render($response, 'pages/utilisateur.twig',array('personnelList' => $this->getAll()));
    }
    public function getAll()
    {
        try
        {
            $employe = $this->container['pdo']->query('SELECT * FROM Utilisateur where enabled = 1 ORDER BY nom')->fetchAll($this->container['pdo']::FETCH_CLASS, '\App\Entity\Utilisateur');
            $am = new Adresse_Controller($this->container);
            foreach ($employe as $e)
            {
                $e->Adresse = $am->describeYourSelf($e->adresse_id);
            }
            return $employe;
        }
        catch(\PDOException $e)
        {
            return "erreur: ".$e;
        }
    }


    private function getOne($id)
    {
        try
        {
            $query = $this->container['pdo']->prepare('SELECT * FROM Utilisateur WHERE id = ?');
            $query->execute([$id]);
            $user = $query->fetchAll($this->container['pdo']::FETCH_CLASS, '\App\Entity\Utilisateur');
            $am = new Adresse_Controller($this->container);
            if (isset($user[0]))
            {
                $user[0]->Adresse = $am->describeYourSelf($user[0]->adresse_id);


                $fc = new Fourniture_Controller($this->container);

                $user[0]->Fourniture = $fc->getAllFromUser($id);
                return $user[0];
            }

            return null;
        }
        catch(\PDOException $e)
        {
            return "erreur: ".$e;
        }
    }
    public function describeYourSelf($id)
    {
        try
        {
            $user = $this->getOne($id);

            return array('id' => $user->id, 'nom' => $user->nom, 'prenom' => $user->prenom);

        }
        catch(\PDOException $e)
        {
            return "erreur: ".$e;
        }
    }

    public function details($request, $response)
    {
        if (!$_SESSION['is_open'])
        {
            echo "<script type='text/javascript'>alert('" . 'Veuillez vous connecter!' . "')</script>";
            $lc = new Login_Controller($this->container);
            $lc->login($request, $response);
        }
        else $this->render($response, 'forms/form_utilisateur.twig',array('utilisateur' => $this->getOne($_POST['id'])));
    }

    public function desactive($id)
    {
        try
        {
            $query = $this->container['pdo']->prepare('UPDATE Utilisateur set enabled = 0 WHERE id = ?');

            $query->execute([$id]);
        }
        catch(\PDOException $e)
        {
            return "erreur: ".$e;
        }
    }

    public function modifier($request, $response)
    {
        try
        {
            $query = $this->container['pdo']->prepare('UPDATE Utilisateur set nom = ?, prenom = ?, email = ? WHERE id = ?');
            $query->execute([$_POST["nom"], $_POST["prenom"], $_POST["email"] ,$_POST["id"]]);
            $am = new Adresse_Controller($this->container);
            $am->modifier($request, $response);
            $this->utilisateur($request, $response);
        }
        catch(\PDOException $e)
        {
            return "erreur: ".$e;
        }
    }

    public function buttons($request, $response)
    {
        //if($_POST['submit'] == 'Annuler')
        if($_POST['submit'] == 'Desactiver')
        {
            $this->desactive($_POST['id']);
        }
        $this->utilisateur($request, $response);
    }

}