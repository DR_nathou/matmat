<?php
/**
 * Created by PhpStorm.
 * salle: drznathou
 * Date: 24/05/2017
 * Time: 13:20
 */

namespace App\Controllers;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

use Slim\App;
use  Slim\Views\Twig;
class Salle_Controller extends Controller
{
    public function salle($request, $response)
    {
        if (!$_SESSION['is_open'])
        {
            echo "<script type='text/javascript'>alert('" . 'Veuillez vous connecter!' . "')</script>";
            $lc = new Login_Controller($this->container);
            $lc->login($request, $response);
        }
        else $this->render($response, 'pages/salle.twig',array('salleList' => $this->getAll() ));
    }

    public function getAll()
    {
        try
        {
            $salle = $this->container['pdo']->query('SELECT * FROM Salle order by numero')->fetchAll($this->container['pdo']::FETCH_CLASS, '\App\Entity\Salle');
            $ts = new TypeSalle_Controller($this->container);
            $ac = new Adresse_Controller($this->container);
            foreach ($salle as $s)
            {
                $s->TypeSalle = $ts->describeYourSelf($s->type_salle_id);
                $s->Adresse = $ac->describeYourSelf($s->adresse_id);
            }
            return $salle;

        }
        catch(\PDOException $e)
        {
            return "erreur: ".$e;
        }
    }

    private function getOne($id)
    {
        try
        {
            $query = $this->container['pdo']->prepare('SELECT * FROM Salle where id = ?');
            $query->execute([$id]);
            $salle = $query->fetchAll($this->container['pdo']::FETCH_CLASS, '\App\Entity\Salle');
            $ts = new TypeSalle_Controller($this->container);
            $ac = new Adresse_Controller($this->container);
            $fc = new Fourniture_Controller($this->container);

            $salle[0]->TypeSalle = $ts->describeYourSelf($salle[0]->type_salle_id);
            $salle[0]->Adresse = $ac->describeYourSelf($salle[0]->adresse_id);
            $salle[0]->Fourniture = $fc->getAllFromSalle($id);

            return $salle[0];
        }
        catch(\PDOException $e)
        {
            return "erreur: ".$e;
        }
    }

    public function describeYourSelf($id)
    {
        try
        {
            $query = $this->container['pdo']->prepare('SELECT id, numero FROM Salle WHERE id = ?');
            $query->execute([$id]);
            $salle = $query->fetchAll($this->container['pdo']::FETCH_CLASS, '\App\Entity\Salle');
            //var_dump($id);
            return array('id' => $salle[0]->id, 'numero' => $salle[0]->numero);

        }
        catch(\PDOException $e)
        {
            return "erreur: ".$e;
        }
    }

    public function details($request, $response)
    {
        if (!$_SESSION['is_open'])
        {
            echo "<script type='text/javascript'>alert('" . 'Veuillez vous connecter!' . "')</script>";
            $lc = new Login_Controller($this->container);
            $lc->login($request, $response);
        }
        else
        {
            $tsc = new TypeSalle_Controller($this->container);

            $this->render($response, 'forms/form_salle.twig',array('salle' => $this->getOne($_POST['id']), 'typeSalle' => $tsc->getAll() ));
        }
    }

    public function modifier($request, $response)
    {
        try
        {
            $query = $this->container['pdo']->prepare('UPDATE Salle set nom = ?, numero = ?, etage = ?, aile = ?, type_salle_id = ? WHERE id = ?');
            $query->execute([$_POST["nom"], $_POST["numero"], $_POST["etage"] , $_POST["aile"], $_POST["typeSalle"] ,$_POST["id"]]);
            $tc = new TypeSalle_Controller($this->container);
            $am = new Adresse_Controller($this->container);
            $tc->modifier($request, $response);
            $am->modifier($request, $response);
            $this->salle($request, $response);
        }
        catch(\PDOException $e)
        {
            return "erreur: ".$e;
        }
    }

    public function nouveau($request, $response)
    {
        if (!$_SESSION['is_open'])
        {
            echo "<script type='text/javascript'>alert('" . 'Veuillez vous connecter!' . "')</script>";
            $lc = new Login_Controller($this->container);
            $lc->login($request, $response);
        }
        else
        {
            $tsc = new TypeSalle_Controller($this->container);

            $this->render($response, 'forms/form_salle.twig', array('new' => true, 'typeSalle' => $tsc->getAll()));
        }
    }

    public function enregistrer($request, $response)
    {
        try
        {
            $am = new Adresse_Controller($this->container);
            $adresse_id = $am->enregistrer($request, $response);

            $query = $this->container['pdo']->prepare('INSERT INTO Salle (type_salle_id, adresse_id, aile, etage, numero, nom) VALUES(?,?,?,?,?,?)');
            $var = $query->execute([$_POST["typeSalle"],$adresse_id , $_POST["aile"], $_POST["etage"], $_POST["numero"], $_POST["nom"]]);

            $this->salle($request, $response);
        }
        catch(\PDOException $e)
        {
            return $e;
        }
    }

    public function supprime($id)
    {
        try
        {
            $query = $this->container['pdo']->prepare('DELETE from Salle WHERE id = ?');
            $query->execute([$id]);

        }catch (\PDOException $e)
        {
            return $e;
        }
    }

    public function buttons($request, $response)
    {
        if($_POST['submit'] == 'Ajouter') $this->nouveau($request, $response);
        if($_POST['submit'] == 'Supprimer')
        {
            $this->supprime($_POST['id']);
            $this->salle($request, $response);
        }

    }
}