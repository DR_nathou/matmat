<?php
/**
 * Created by PhpStorm.
 * User: drznathou
 * Date: 24/05/2017
 * Time: 13:24
 */

namespace App\Controllers;


class Profile_Controller extends Controller
{
    public function profile($request, $response)
    {
        if (!$_SESSION['is_open'])
        {
            echo "<script type='text/javascript'>alert('" . 'Veuillez vous connecter!' . "')</script>";
            $lc = new Login_Controller($this->container);
            $lc->login($request, $response);
        }
        else $this->render($response, 'pages/profile.twig',array());
    }
    private function getProfileName()
    {
        try{

            $result = $this->container['pdo']->query("select * from Adresse");
            $value = $result->fetchObject( $class_name = "\App\Entity\Adresse", array() );

            return $value;
        }
        catch(\PDOException $e)
        {
            return "erreur: ".$e;
        }
    }
}