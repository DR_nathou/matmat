<?php
namespace App\Controllers;
//use Slim\App;

/**
 * Created by PhpStorm.
 * User: drznathou
 * Date: 02/02/2017
 * Time: 12:55
 */

class Contact_Manager
{
    static function _get_contact($container)
    {
        try
        {
            $result = $container['pdo']->query("select * from Utilisateur");
            $data = $result->fetchAll(\PDO::FETCH_OBJ);
            $contactList = array();
            foreach ($data as $value) {
                $obj = new \App\DD\Contact($value->nom, $value->prenom, $value->email, $value->adresse, $value->telephone, $value->id_login, $value->id_contact, $container);

                array_push($contactList, $obj);
            }
        }
        catch(\PDOException $e)
        {
            return "erreur: ".$e;
        }
       return $contactList;
    }
    static function _update($contact)
    {
        try
        {
            $pdoQuery = $contact->container['pdo']->prepare("UPDATE contact SET nom=?, prenom=?, email=?, telephone=?, adresse=? WHERE id_contact=?");

            $pdoQuery->execute([$contact->nom, $contact->prenom, $contact->email, $contact->telephone, $contact->adresse, $contact->idcontact]);
        }
        catch(\PDOexception $e)
        {
            return "Erreur:".$e.". Veuillez réessayer.";
        }
        return array('msg' => "Contact mis à jour", 'good' => true);

    }

    static function _new($contact)
    {
        if(self::_exist($contact)) return array('msg' => "Un contact similaire existe déja", 'good' => false);
        
        try
        {
            $pdoQuery = $contact->container['pdo']->prepare("INSERT INTO contact (nom, prenom, email, telephone, adresse, id_login) VALUES(?,?,?,?,?,?)");

            $pdoQuery->EXECUTE([$contact->nom, $contact->prenom, $contact->email, $contact->telephone, $contact->adresse, 0]);
        }
        catch (\PDOException $e)
        {
            return "Erreur:".$e." veuillez réessayer";
        }
        return array('msg' => "Nouveau Contact ajouté avec succes!", 'good' => true);
    }
    static function _delete($contact)
    {
        try
        {
            $pdoQuery = $contact->container['pdo']->prepare("DELETE FROM contact WHERE id_contact=?");

            $pdoQuery->execute([$contact->idcontact]);
        }
        catch (\PDOException $e)
        {
            return "Erreur:".$e." veuillez réessayer";
        }
        return "Contact suprimme!";
    }
static function _exist($contact)
    {
        $contactList = self::_get_contact($contact->container);
        foreach ($contactList as $con)
        {
            if($contact->nom == $con->nom &&
            $contact->prenom == $con->prenom &&
            $contact->adresse == $con->adresse) return true;
        }

        return false;
    }
}