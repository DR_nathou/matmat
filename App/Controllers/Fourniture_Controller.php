<?php
/**
 * Created by PhpStorm.
 * User: drznathou
 * Date: 24/05/2017
 * Time: 13:22
 */

namespace App\Controllers;

use App\Entity\Type;
use App\Managers\BarCode_Manager;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

use Slim\App;
use  Slim\Views\Twig;

class Fourniture_Controller extends Controller
{
    public function fourniture($request, $response, $fourniture = null)
    {
        if (!$_SESSION['is_open'])
        {
            echo "<script type='text/javascript'>alert('" . 'Veuillez vous connecter!' . "')</script>";
            $lc = new Login_Controller($this->container);
            $lc->login($request, $response);
        }
        else
        {
            if ($fourniture == null) $fourniture = $this->getAll();
            $this->render($response, 'pages/fourniture.twig', array('fournitureList' => $fourniture));
        }


    }

    public function getAll()
    {
        try
        {
            $fourniture = $this->container['pdo']->query('SELECT * FROM Fourniture')->fetchAll($this->container['pdo']::FETCH_CLASS, '\App\Entity\Fourniture');
            $pc = new Utilisateur_Controller($this->container);
            $sc = new Salle_Controller($this->container);
            $dc = new Description_Controller($this->container);
            foreach ($fourniture as $f)
            {
                $f->Utilisateur = $pc->describeYourSelf($f->utilisateur_id);
                $f->Salle = $sc->describeYourSelf($f->salle_id);
                $f->Description = $dc->describeYourSelf($f->description_id);
                //$this->generateCode($f->id);
            }

            return $fourniture;
        }
        catch(\PDOException $e)
        {
            return "erreur: ".$e;
        }
    }

    private function getInformatique()
    {
        try
        {
            $fourniture = $this->container['pdo']->query('SELECT * FROM Fourniture f INNER JOIN Description d on f.description_id = d.id WHERE d.type_id = 1 or d.type_id = 3')->fetchAll($this->container['pdo']::FETCH_CLASS, '\App\Entity\Fourniture');

            $pc = new Utilisateur_Controller($this->container);
            $sc = new Salle_Controller($this->container);
            $dc = new Description_Controller($this->container);
            foreach ($fourniture as $f)
            {
                $f->Utilisateur = $pc->describeYourSelf($f->utilisateur_id);
                $f->Salle = $sc->describeYourSelf($f->salle_id);
                $f->Description = $dc->describeYourSelf($f->description_id);
            }
            return $fourniture;
        }
        catch(\PDOException $e)
        {
            return "erreur: ".$e;
        }
    }
    private function getOther()
    {
        try
        {
            $fourniture = $this->container['pdo']->query('SELECT * FROM Fourniture f INNER JOIN Description d on f.description_id = d.id WHERE d.type_id = 2 or d.type_id = 4')->fetchAll($this->container['pdo']::FETCH_CLASS, '\App\Entity\Fourniture');

            $pc = new Utilisateur_Controller($this->container);
            $sc = new Salle_Controller($this->container);
            $dc = new Description_Controller($this->container);
            foreach ($fourniture as $f)
            {
                $f->Utilisateur = $pc->describeYourSelf($f->utilisateur_id);
                $f->Salle = $sc->describeYourSelf($f->salle_id);
                $f->Description = $dc->describeYourSelf($f->description_id);
            }
            return $fourniture;
        }
        catch(\PDOException $e)
        {
            return "erreur: ".$e;
        }
    }

    public function getAllFromUser($id)
    {
        try
        {
            $query = $this->container['pdo']->prepare('SELECT * FROM Fourniture WHERE utilisateur_id = ?');
            $query->execute([$id]);
            $fourniture = $query->fetchAll($this->container['pdo']::FETCH_CLASS, '\App\Entity\Fourniture');
            $sc = new Salle_Controller($this->container);
            $dc = new Description_Controller($this->container);
            foreach ($fourniture as $f)
            {
                $f->Salle = $sc->describeYourSelf($f->salle_id);
                $f->Description = $dc->describeYourSelf($f->description_id);
            }
            return $fourniture;

        }
        catch(\PDOException $e)
        {
            return "erreur: ".$e;
        }
    }


    public function getAllFromSalle($id)
    {
        try
        {
            $query = $this->container['pdo']->prepare('SELECT * FROM Fourniture WHERE salle_id = ?');
            $query->execute([$id]);
            $fourniture = $query->fetchAll($this->container['pdo']::FETCH_CLASS, '\App\Entity\Fourniture');
            $dc = new Description_Controller($this->container);
            $pc = new Utilisateur_Controller($this->container);
            foreach ($fourniture as $f)
            {
                $f->Utilisateur = $pc->describeYourSelf($f->utilisateur_id);
                $f->Description = $dc->describeYourSelf($f->description_id);
            }
            return $fourniture;

        }
        catch(\PDOException $e)
        {
            return "erreur: ".$e;
        }
    }

    private function getOne($id)
    {
        try
        {
            $query = $this->container['pdo']->prepare('SELECT * FROM Fourniture WHERE id = ?');
            $query->execute([$id]);
            $f = $query->fetchAll($this->container['pdo']::FETCH_CLASS, '\App\Entity\Fourniture');
            $pc = new Utilisateur_Controller($this->container);
            $sc = new Salle_Controller($this->container);
            $dc = new Description_Controller($this->container);

            if(isset($f[0]))
            {
                $f[0]->Utilisateur = $pc->describeYourSelf($f[0]->utilisateur_id);
                $f[0]->Salle = $sc->describeYourSelf($f[0]->salle_id);
                $f[0]->Description = $dc->describeYourSelf($f[0]->description_id);

                return $f[0];
            }
        }
        catch(\PDOException $e)
        {
            return "erreur: ".$e;
        }
    }

    public function details($request, $response)
    {
        if (!$_SESSION['is_open'])
        {
            echo "<script type='text/javascript'>alert('" . 'Veuillez vous connecter!' . "')</script>";
            $lc = new Login_Controller($this->container);
            $lc->login($request, $response);
        }
        else
        {
            $sc = new Salle_Controller($this->container);

            $tfc = new Type_Controller($this->container);
            $uc = new Utilisateur_Controller($this->container);
            $this->render($response, 'forms/form_fourniture.twig', array('new' => false, 'fourniture' => $this->getOne($_POST['id']), 'salleList' => $sc->getAll(), 'typeFourniture' => $tfc->getAll(), 'utilisateurList' => $uc->getAll()));
        }
    }


    public function buttons($request, $response)
    {

        if($_POST["submit"] == 'Supprimer')
        {
            $this->supprime($_POST['id']);
            $this->fourniture($request, $response);
        }
        elseif($_POST["submit"] == "Informatique") $this->fourniture($request, $response, $this->getInformatique());
        elseif($_POST["submit"] == "Non informatique") $this->fourniture($request, $response, $this->getOther());
        elseif($_POST['submit'] == "Ajouter") $this->nouveau($request, $response);
        else
        {
            $this->fourniture($request, $response);
        }

    }

    public function nouveau($request, $response)
    {
        if (!$_SESSION['is_open'])
        {
            echo "<script type='text/javascript'>alert('" . 'Veuillez vous connecter!' . "')</script>";
            $lc = new Login_Controller($this->container);
            $lc->login($request, $response);
        }
        else
        {
            $sc = new Salle_Controller($this->container);
            $tfc = new Type_Controller($this->container);
            $uc = new Utilisateur_Controller($this->container);
            $this->render($response, 'forms/form_fourniture.twig', array('new' => true, 'salleList' => $sc->getAll(), 'typeFourniture' => $tfc->getAll(), 'utilisateurList' => $uc->getAll()));
        }
    }

    public function enregistrer($request, $response)
    {
        try
        {
            $dc = new Description_Controller($this->container);
            $description_id = $dc->enregistrer();
            $query = $this->container['pdo']->prepare('INSERT INTO  Fourniture ( utilisateur_id ,  salle_id ,  description_id ,  marque ,  modele ) VALUES (?, ?, ?, ?, ?)');
            $query->execute([$_POST['utilisateur_id'], $_POST['salle_id'], $description_id, $_POST['marque'], $_POST['modele']]);

            $query = $this->container['pdo']->prepare('select id from Fourniture where utilisateur_id = ? and salle_id = ? and description_id = ? and  marque = ? and  modele = ?');
            $query->execute([$_POST['utilisateur_id'], $_POST['salle_id'], $description_id, $_POST['marque'], $_POST['modele']]);

            $f = $query->fetchAll($this->container['pdo']::FETCH_CLASS, '\App\Entity\Fourniture');
            $this->generateCode($f[0]->id);

            $this->fourniture($request, $response);
        }
        catch(\PDOException $e)
        {
            return $e;
        }
    }

    public function supprime($id)
    {
        try
        {
            $query = $this->container['pdo']->prepare('DELETE from Fourniture WHERE id = ?');
            $query->execute([$id]);

        }catch (\PDOException $e)
        {
            return $e;
        }
    }
    public function modifier($request, $response)
    {
        try
        {
            $query = $this->container['pdo']->prepare('UPDATE Fourniture set marque = ?, modele = ?, salle_id = ?, utilisateur_id = ? WHERE id = ?');
            $query->execute([$_POST["marque"], $_POST["modele"], $_POST["salle_id"], $_POST["utilisateur_id"] ,$_POST["id"]]);
            $dc = new Description_Controller($this->container);
            $dc->modifier();
            $this->generateCode($_POST["id"]);
            $this->fourniture($request, $response);
        }
        catch(\PDOException $e)
        {
            return $e;
        }
    }

    private function generateCode($id)
    {
        try
        {
            $query = $this->container['pdo']->prepare('UPDATE Fourniture set code = cast((CONCAT(id, utilisateur_id, salle_id, description_id)) as UNSIGNED) WHERE id = ?');
            $query->execute([$id]);
        }
        catch(\PDOException $e)
        {
            return $e;
        }
    }
}
