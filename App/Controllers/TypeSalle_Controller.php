<?php
/**
 * Created by PhpStorm.
 * User: drznathou
 * Date: 26/05/2017
 * Time: 04:04
 */

namespace App\Controllers;


class TypeSalle_Controller extends Controller
{

    public function getAll()
    {
        return $this->container['pdo']->query('SELECT * FROM TypeSalle')->fetchAll($this->container['pdo']::FETCH_CLASS, '\App\Entity\TypeSalle');
    }

    public function describeYourSelf($id)
    {
        try
        {
            $query = $this->container['pdo']->prepare('SELECT id, nom FROM TypeSalle WHERE id = ?');
            $query->execute([$id]);
            $user = $query->fetchAll($this->container['pdo']::FETCH_CLASS, '\App\Entity\TypeSalle');
            //var_dump($id);
            return array('id' => $user[0]->id, 'nom' => $user[0]->nom);

        }
        catch(\PDOException $e)
        {
            return "erreur: ".$e;
        }
    }


    public function nouveau($request, $response)
    {
        if (!$_SESSION['is_open'])
        {
            echo "<script type='text/javascript'>alert('" . 'Veuillez vous connecter!' . "')</script>";
            $lc = new Login_Controller($this->container);
            $lc->login($request, $response);
        }
        else $this->render($response, 'forms/form_type.twig',array('typeSalle' => true, 'typeList' => $this->getAll() ));
    }

    public function changeNom($request, $response)
    {
        if (!$_SESSION['is_open'])
        {
            echo "<script type='text/javascript'>alert('" . 'Veuillez vous connecter!' . "')</script>";
            $lc = new Login_Controller($this->container);
            $lc->login($request, $response);
        }
        else $this->render($response, 'forms/form_type.twig',array('nom' => $_POST['nom'], 'id' => $_POST['id'],'modifier' => true, 'typeSalle' => true, 'typeList' => $this->getAll()));
    }

    public function enregistrer($request, $response)
    {
        try
        {
            $query = $this->container['pdo']->prepare('INSERT INTO TypeSalle (nom) VALUES(?)');
            $query->execute([$_POST['nom']]);
            $this->nouveau($request, $response);
        }
        catch(\PDOException $e)
        {
            return $e->getMessage();
        }
    }

    public function modifier($request, $response)
    {
        try
        {
            $query = $this->container['pdo']->prepare('UPDATE TypeSalle set nom = ? WHERE id = ?');
            $query->execute([$_POST['nom'] ,$_POST['id']]);

            $this->nouveau($request, $response);
        }
        catch(\PDOException $e)
        {
            return $e->getMessage();
        }
    }

}