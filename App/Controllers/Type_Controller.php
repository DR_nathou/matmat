<?php
/**
 * Created by PhpStorm.
 * User: drznathou
 * Date: 26/05/2017
 * Time: 04:04
 */

namespace App\Controllers;


class Type_Controller extends Controller
{
    public function getAll()
    {
        try
        {
            $salle = $this->container['pdo']->query('SELECT * FROM Type ORDER BY nom ')->fetchAll($this->container['pdo']::FETCH_CLASS, '\App\Entity\Type');
            $ts = new TypeSalle_Controller($this->container);
            $ac = new Adresse_Controller($this->container);
            foreach ($salle as $s)
            {
                $s->TypeSalle = $ts->describeYourSelf($s->type_salle_id);
                $s->Adresse = $ac->describeYourSelf($s->adresse_id);
            }
            return $salle;

        }
        catch(\PDOException $e)
        {
            return "erreur: ".$e;
        }
    }
    public function describeYourSelf($id)
    {
        try
        {
            $query = $this->container['pdo']->prepare('SELECT nom FROM Type WHERE id = ?');
            $query->execute([$id]);
            $Type = $query->fetchAll($this->container['pdo']::FETCH_CLASS, '\App\Entity\Type');
            //var_dump($id);
            return $Type[0];

        }
        catch(\PDOException $e)
        {
            return "erreur: ".$e;
        }
    }

    public function nouveau($request, $response)
    {
        if (!$_SESSION['is_open'])
        {
            echo "<script type='text/javascript'>alert('" . 'Veuillez vous connecter!' . "')</script>";
            $lc = new Login_Controller($this->container);
            $lc->login($request, $response);
        }
        else $this->render($response, 'forms/form_type.twig',array('typeList' => $this->getAll() ));
    }

    public function changeNom($request, $response)
    {
        if (!$_SESSION['is_open'])
        {
            echo "<script type='text/javascript'>alert('" . 'Veuillez vous connecter!' . "')</script>";
            $lc = new Login_Controller($this->container);
            $lc->login($request, $response);
        }
        else $this->render($response, 'forms/form_type.twig',array('nom' => $_POST['nom'], 'id' => $_POST['id'],'modifier' => true, 'typeList' => $this->getAll()));
    }

    public function enregistrer($request, $response)
    {
        try
        {
            $query = $this->container['pdo']->prepare('INSERT INTO Type (nom) VALUES(?)');
            $query->execute([$_POST['nom']]);
            $this->nouveau($request, $response);
        }
        catch(\PDOException $e)
        {
            return $e->getMessage();
        }
    }
    public function modifier($request, $response)
    {
        try
        {
            $query = $this->container['pdo']->prepare('UPDATE Type set nom = ? WHERE id = ?');
            $query->execute([$_POST['nom'] ,$_POST['id']]);
            $this->nouveau($request, $response);
        }
        catch(\PDOException $e)
        {
            return $e->getMessage();
        }
    }

}